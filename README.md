# Algorithms 4th edition 공부 프로젝트

## 개요

- Algorithms 4th edition (로버트 세지윅, 케빈 웨인 지음 | 권오인 옮김 | 길벗) 책을 공부하며 기록도 남길 겸 실습의 목적으로 본 프로젝트를 개설하였습니다.
- 작성 언어는 C입니다. 곧 프로그래밍 관련 학교에 들어가서 그거 준비할 겸.. 오랜만에 C를 접하니 감회가 새롭습니다.
- main.c 의 main 함수가 실행 시작점입니다.

## 구동 방법

1. git 및 Visual Studio 설치 (Visual Studio는 C++ 개발환경까지 되어있어야 함)
1. 적당한 폴더에 들어가서 `git clone https://gitlab.com/EzKorry/algorithm-learn.git` 명령 입력
1. 데이터 파일 다운로드 (하단 링크 참조)
1. `main.c` 파일의 최상단에서 데이터 파일 경로 수정
1. `Ctrl+F5` (Start Without Debugging) 실행

## 추가 조치 사항

- 일단 프로젝트에서는 [GNU Scientific Library(이하 gsl)](https://www.gnu.org/software/gsl/)를 포함하도록 하고 있으나 사용하지는 않았습니다. 이미 gsl이 세팅되어 있으나, 혹시나 제대로 세팅되지 않은 경우를 위해 설치법을 첨가합니다.
   - https://www.bruot.org/hp/libraries/ 접속
   - GNU Scientific Library binaries - GSL2.2 - For Visual Studio Community MSVC 2017 (Release, 64 bits) 다운로드 ([Direct Link](https://www.bruot.org/hp/media/files/libraries/gsl_2_2_msvc2017_64.zip))
   - `/algs4-vscodeproj` 바로 아래에 `lib` 폴더와 `include` 폴더를 옮깁니다. (아마도 솔루션 설정이 남아있기 때문에 경로는 잘 읽지 않을까 예상합니다.)
   - 경로가 기본적으로 설정되어 있겠으나, 만약 제대로 설정되어 있지 않은 경우 아래 두 가지를 시행하세요
      - `Project Properties - C/C++ - General - Additional Include Directories` 에서, `.\include\\` 설정.
      - `Project Properties - Linker - General - Additional Library Directoreis` 에서 `.\lib\gsl\` 설정.

## 외부 링크

- [Algorithms 4th edition 다운로드 리소스 페이지](https://algs4.cs.princeton.edu/code/)
- [데이터 파일 즉시 다운로드](https://algs4.cs.princeton.edu/code/algs4-data.zip)
- [Visual Studio 2017에서 GLS(GNU Scientific Library) 사용하기(블로그)(gsl 세팅시 참고)](https://webnautes.tistory.com/1323)