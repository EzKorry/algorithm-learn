#include <stdbool.h>

void sort(char** a, int length);

void sort_i(int* a, int length);

void sort_down_merge(char** a, int lo, int hi);

void quick_fast3way_sort(char** a, int lo, int hi);

void quick_sort(char** a, int lo, int hi);

void quick_sort_i(int* a, int lo, int hi);

int quick_partition(char** a, int lo, int hi);

int quick_partition_i(int* i, int lo, int hi);

void heap_sort(char** a, int length);

void heap_swim(char** a, int k);

void heap_sink(char** a, int k, int length);

bool less(char* a, char* b);

bool less_i(int a, int b);

void exch(char** a, int i, int j);

void exch_i(int* a, int i, int j);

void show(char** a, int length);

void show_i(int* a, int length);

bool isSorted(char** a, int length);

bool isSorted_i(int* a, int length);

void merge(char** a, int lo, int mid, int hi);

char** aux;

int sort_length;