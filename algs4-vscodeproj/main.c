#include <stdio.h>
#include "file-reader.h"
#include "uf.h"
#include "sort.h"
#include "redblackbst.h"
#include <sys\timeb.h> 
#include <assert.h>
#include <stdlib.h>
#include <string.h>


//const char* DATA_FILE_PATH = "C:\\Users\\tooth\\Desktop\\algs4\\algs4-data\\largeUF.txt";
//const char* DATA_FILE_PATH = "C:\\Users\\tooth\\Desktop\\algs4\\algs4-data\\leipzig1M.txt";
const char* DATA_FILE_PATH = "C:\\Users\\tooth\\Desktop\\algs4\\algs4-data\\tinyTale.txt";
FILE* fp;

struct timeb timer_start, timer_end;


int frequency_counter() {
	int count = 0;
	/*while (!feof(fp)) {
		count++;
		char* word = f_readString_mkptr(fp); 
		if (word[0] == '\0') break;
		d_add_wordcount(word);
	}*/
	d_printall();
	while (true) {
		printf("행동을 골라주세요. 1:삽입 2:삭제 5:최댓값 삭제 6:최소값 삭제 --> ");
		int input = 0;
		scanf_s("%d", &input);
		char key[20];
		switch (input) {
		case 1: // 삽입
			printf("키를 입력해주세요 -->");
			scanf_s("%s", key, 20);
			char* o = (char*)malloc(sizeof(char) * 20);
			strcpy_s(o, 20, key);
			d_add_wordcount(o);
			d_printall();

			break;
		case 2: // 삭제
			printf("키를 입력해주세요 -->");
			scanf_s("%s", key, 20);
			d_delete(key);
			d_printall();
			break;

		case 5: // 최댓값 삭제
			d_delete_max();
			d_printall();
			break;
		case 6: // 최솟값 삭제
			d_delete_min();
			d_printall();
			break;

		default:
			break;
		}
	}
	

	
	return 0;
}



void start_timer() {
	ftime(&timer_start);
}

void finish_timer() {
	ftime(&timer_end);
	
}

int get_elapsed() {
	return (int)(1000.0 * (timer_end.time - timer_start.time)
		+ (timer_end.millitm - timer_start.millitm));
}


int uf_main() {

	int n = 0;
	f_readInt(fp, &n);

	uf_init(n);
	int i = 0;
	while (!feof(fp)) {

		int p, q;
		f_readInt(fp, &p);
		f_readInt(fp, &q);
		i++;
		if (uf_connected(p, q)) {
			continue;
		}
		uf_union(p, q);
		printf("%d : %d %d\n", i, p, q);
	}
	printf("%d\n", i);

	printf("%d components", uf_count);
	
	

	return 0;

}

void sort_data_gathering(char*** strings, int* count) {
	f_readAllStrings(fp, strings, count);
}

void sort_data_gathering_i(int** ints, int* count) {
	
}

int sort_main() {

	char** strings = NULL;
	int* ints = NULL;
	int count = 0;

	
	// ---- sort string data ----
	sort_data_gathering(&strings, &count);
	sort_length = count;
	
	show(strings, count);
	printf("word count: %d\n", count);
	start_timer();
	sort(strings, count);
	finish_timer();
	
	//assert(isSorted(strings, count));
	show(strings, count);
	printf("\n정렬에 %u 밀리초가 소요되었습니다.\n", get_elapsed());

	// ---- sort_int_data ----
	//sort_data_gathering_i(&ints, &count);
	//sort_length = count;

	//show_i(ints, count);
	//printf("word count: %d\n", count);
	//sort_i(ints, count);
	//assert(isSorted_i(ints, count));
	//show_i(ints, count);

	// ------------

	printf("count: %d\n", count);
	return 0;
}

// 이 함수를 계속해서 수정해주세요.
int execute() {
	return frequency_counter();
}

int main() {
	fopen_s(&fp, DATA_FILE_PATH, "r");
	int result = execute();
	fclose(fp);    // 파일 포인터 닫기
	return result;
}

