#include "uf.h"
#include <stdlib.h>
#include <memory.h>
int sum(int a, int b) {
	return a + b;
}

void uf_init(int n)
{
	uf_id = (int*)malloc(sizeof(int) * n);
	for (int i = 0; i < n; i++) {
		uf_id[i] = i;
	}
	uf_sz = (int*)malloc(sizeof(int) * n);
	memset(uf_sz, 1, sizeof(int) * n);
	/*for (int i = 0; i < n; i++) {
		uf_sz[i] = 1;
	}*/

	uf_count = n;
	uf_length = n;
}

int uf_get_count()
{
	return uf_count;
}

bool uf_connected(int p, int q)
{
	return uf_find(p) == uf_find(q);
}

/* quick-find */
/*
int uf_find(int p)
{
	return uf_id[p];
}

void uf_union(int p, int q)
{
	int pID = uf_find(p);
	int qID = uf_find(q);
	if (pID == qID) {
		return;
	}

	for (int i = 0; i < uf_length; i++) {
		if (uf_id[i] == pID) uf_id[i] = qID;
	}
	uf_count--;
}
*/
int uf_find(int p) {
	int pp = p;
	while (p != uf_id[p]) p = uf_id[p];
	while (pp != uf_id[pp]) {
		int g = pp;
		pp = uf_id[pp];
		uf_id[g] = p;
	}
	return p;
}

void uf_union(int p, int q) {
	int i = uf_find(p);
	int j = uf_find(q);
	if (i == j) return;

	if (uf_sz[i] < uf_sz[j]) {
		uf_id[i] = j;
		uf_sz[j] += uf_sz[i];
	}
	else {
		uf_id[j] = i;
		uf_sz[i] += uf_sz[j];
	}
	uf_count--;
}
