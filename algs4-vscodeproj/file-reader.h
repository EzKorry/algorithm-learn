#pragma once
#include <stdio.h>
#include <stdbool.h>

void f_readInt(FILE* fp, int* opt);

void f_readAllInts(FILE* fp, int** opt_ints, int* opt_count);

char* f_readString_mkptr(FILE* fp);

int f_readString(char* buffer, FILE* fp);

void f_readAllStrings(FILE* fp, char*** opt_strings, int* opt_count);

char* f_readLine(char* buffer, FILE* fp);

bool f_isEmpty(FILE* fp);

void f_open(FILE* fp, const char* path);
