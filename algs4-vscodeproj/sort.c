#include "sort.h"
#include <string.h>
#include <stdlib.h>


void sort(char** a, int length)
{
	////insertion sort
	//for (int i = 1; i < length; i++) {
	//	for (int j = i; j > 0 && less(a[j], a[j - 1]); j--) {
	//		exch(a, j, j - 1);
	//	}
	//}


	//selection sort
	/*for (int i = 0; i < length; i++) {
		printf("%d is sorting\n", i);
		int min = i;
		for (int j = i + 1; j < length; j++) {
			if (less(a[j], a[min])) {
				min = j;
			}
		}
		exch(a, i, min);
	}*/


	// shell sort
	//int h = 1;
	//while (h < length / 3) h = 3 * h + 1;
	//while (h >= 1) {
	//	for (int i = h; i < length; i++) {
	//		printf("h:%d, i:%d\n", h, i);
	//		for (int j = i; j >= h && less(a[j], a[j - h]); j -= h) {
	//			exch(a, j, j - h);
	//		}
	//	}
	//	h /= 3;
	//}

	// merge sort
	//aux = (char**)malloc(sizeof(char*) * length);
	//sort_down_merge(a, 0, length - 1);


	//quick sort
	//quick_sort(a, 0, length - 1);

	//fast 3way quick sort
	//quick_fast3way_sort(a, 0, length - 1);

	// heap sort
	heap_sort(a, length);
}

// strcmp(a, b) 결과
// -1 : a < b
// 0  : a == b
// 1  : a < b

void quick_fast3way_sort(char** a, int lo, int hi) {
	printf("%07d to %07d starts\n", lo, hi);
	if (lo >= hi) return;

	int p = lo, i = lo-1, q = hi, j = hi + 1;
	//while (strcmp(a[++p], a[lo])) 
	while (true) {
		while (++i <= hi) { //i가 hi보다 크다면 즉시 멈춘다.

			// 비교 실시
			int cmp = strcmp(a[i], a[lo]);
			
			// a[i] < a[lo] 일 때에는 아무것도 하지 않는다.
			// a[i] > a[lo] 이면 a[i]와 a[j]를 바꿀 준비를 해야 한다.
			if (cmp > 0 ) break;  

			// a[i] == a[lo] 이고, j가 먼저 검사를 안했다면 a[i]와 a[p++]를 교환한다.
			else if (cmp == 0 && i < j) exch(a, i, p++); 
		}
		while (--j >= lo) { // j가 lo보다 작다면 즉시 멈춘다.
			// 비교 실시
			int cmp = strcmp(a[lo], a[j]);

			// a[lo] < a[j] 일 때에는 아무것도 하지 않는다.
			// a[lo] > a[j] 이면 a[i]와 a[j]를 바꿀 준비를 해야 한다.
			if (cmp > 0) break; 
			
			// a[lo] == a[j] 이고, i가 먼저 검사를 안했다면 a[j]와 a[q--]를 교환한다.
			else if (cmp == 0 && i < j) exch(a, j, q--);
		}
		if (i >= j) break;
		exch(a, i, j);
	}
	int left_same_count = p - lo;
	int right_same_count = hi - q;
	int leftmove = min(j- p + 1, left_same_count);
	int rightmove = min(q - i + 1, right_same_count);

	int mj = j, mi = i;

	while (leftmove-- > 0 && mj >= lo) exch(a, lo + leftmove, mj--);
	while (rightmove-- > 0 && mi <= hi) exch(a, hi - rightmove, mi++);

	j -= left_same_count;
	i += right_same_count;
	
	quick_fast3way_sort(a, lo, j);
	quick_fast3way_sort(a, i, hi);
}

void quick_sort(char** a, int lo, int hi) {
	if (hi <= lo) return;
	int j = quick_partition(a, lo, hi);
	//show(a, sort_length);
	quick_sort(a, lo, j - 1);
	quick_sort(a, j + 1, hi);
}

int quick_partition(char** a, int lo, int hi) {
	printf("lo:%d, hi:%d\n", lo, hi);
	int l = lo + 1, r = hi;
	while (true) {
		while (less(a[l], a[lo])) {
			l++;
			if (l == hi) break;
		}
		while (less(a[lo], a[r])) {
			r--;
			if (r == lo) break;
		}
		if (l >= r) break;
		exch(a, l, r);
		l++;
		r--;
	}
	exch(a, r, lo);
	return r;
}

void sort_down_merge(char** a, int lo, int hi) {
	if (hi <= lo) return;
	int mid = lo + (hi - lo) / 2;
	sort_down_merge(a, lo, mid);
	sort_down_merge(a, mid + 1, hi);
	merge(a, lo, mid, hi);
}

void heap_sort(char** a, int length)
{
	for (int i = length / 2; i >= 1; i--) {
		heap_sink(a, i, length);
		printf("heap composing: %d\n", i);
	}
	int l = length;
	while (l > 0) {
		exch(a, --l, 0);
		printf("heap aligning: %d\n", l);
		heap_sink(a, 1, l);
	}
}

void heap_swim(char** a, int k)
{
	while (k > 1 && less(a[k / 2 - 1], a[k - 1])) {
		exch(a, k / 2 - 1, k - 1);
		k /= 2;
	}
}

void heap_sink(char** a, int k, int length)
{
	while (k * 2 <= length) {
		int j = k * 2;
		if (j < length - 1 && less(a[j - 1], a[j])) j++;
		if (less(a[j - 1], a[k - 1])) break;
		exch(a, k - 1, j - 1);
		k = j;
	}
}

bool less(char* a, char* b)
{
	return strcmp(a, b) < 0;
}

void exch(char** a, int i, int j)
{
	char* temp = a[i];
	a[i] = a[j];
	a[j] = temp;
}

bool isSorted(char** a, int length)
{
	for (int i = 1; i < length; i++) {
		if (less(a[i], a[i - 1])) return false;
	}
	return true;
}

void show(char** a, int length)
{
	for (int i = 0; i < length; i++) {
		printf("%s ", a[i]);
	}
	printf("\n");
}

void merge(char** a, int lo, int mid, int hi) {
	//printf("merging %d to %d\n", lo, hi);
	int sl = lo, sr = mid + 1;

	for (int i = lo; i <= hi; i++) {
		aux[i] = a[i];
	}

	for (int i = lo; i <= hi; i++) {
		if (sl > mid) a[i] = aux[sr++];
		else if (sr > hi) a[i] = aux[sl++];
		else if (less(aux[sr], aux[sl])) a[i] = aux[sr++];
		else a[i] = aux[sl++];
	}

	show(a, sort_length);
}
