#include "redblackbst.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

const bool BLACK = false;
const bool RED = true;

rbnode* bst_root = NULL;
int print_lv = 0;

rbnode* rbNode(rbKey key, rbValue value, int sub_count, bool color)
{
	rbnode* k = (rbnode*)malloc(sizeof(rbnode));
	k->key = key;
	k->value = value;
	k->sub_count = sub_count;
	k->color = color;
	k->left = NULL;
	k->right = NULL;
	return k;
}

bool d_isred(rbnode* h)
{
	if (h == NULL) return false;
	return h->color == RED;
}

rbnode* d_rotateLeft(rbnode* h)
{
	rbnode* x = h->right;
	h->right = x->left;
	x->left = h;
	x->color = h->color;
	h->color = RED;
	x->sub_count = h->sub_count;
	h->sub_count = 1 + d_size(h->left) + d_size(h->right);
	printf("rotated left %s --> %s\n", h->key, x->key);
	return x;
}

rbnode* d_rotateRight(rbnode* h)
{
	rbnode* x = h->left;
	h->left = x->right;
	x->right = h;
	x->color = h->color;
	h->color = RED;
	x->sub_count = h->sub_count;
	h->sub_count = 1 + d_size(h->left) + d_size(h->right);
	printf("rotated right %s --> %s\n", h->key, x->key);
	return x;
}

int d_size(rbnode* h)
{
	if (h == NULL) return 0;
	else return h->sub_count;
}

void tree_put(rbKey key, rbValue value)
{
	bst_root = d_put(bst_root, key, value);
	bst_root->color = BLACK;
}

rbnode* d_put(rbnode* node, rbKey key, rbValue value)
{
	if (node == NULL) return rbNode(key, value, 1, RED);
	int cmp = strcmp(key, node->key);
	if (cmp < 0) node->left = d_put(node->left, key, value);
	else if (cmp > 0) node->right = d_put(node->right, key, value);
	else node->value = value;

	if (d_isred(node->right) && !d_isred(node->left)) node = d_rotateLeft(node);
	if (d_isred(node->left) && d_isred(node->left->left)) node = d_rotateRight(node);
	if (d_isred(node->left) && d_isred(node->right)) d_flip_colors(node);

	node->sub_count = d_size(node->left) + d_size(node->right) + 1;
	return node;
}

bool d_contains(rbKey key)
{
	return d_find(bst_root, key) != NULL;
}

void* d_get(rbKey key)
{
	rbnode* found = d_find_root(key);
	if (found) return found->value;
	else return NULL;
}

rbnode* d_min(rbnode* h)
{
	if (h->left == NULL) return h;
	return d_min(h->left);
}

rbnode* d_find(rbnode* x, rbKey key)
{
	if (x == NULL) return NULL;
	int cmp = strcmp(key, x->key);
	if (cmp < 0) return d_find(x->left, key);
	else if (cmp > 0) return d_find(x->right, key);
	else return x;
}

rbnode* d_find_root(rbKey key)
{
	return d_find(bst_root, key);
}

rbnode* d_move_red_left(rbnode* h)
{
	printf("start move red left %s\n", h->key);
	// h는 레드이고, h.left와 h.left.left는 블랙이라고 가정한다. (h는 다른 누군가와 붙어있고, h.
	// h.left 또는 그 자식 중 하나를 레드로 만든다.
	d_flip_colors(h);

	if (d_isred(h->right->left)) { // 현재 노드가 4-노드일 때 큰 값 두 개를 분리한다.
		h->right = d_rotateRight(h->right);
		h = d_rotateLeft(h);
		d_flip_colors(h);
	}
	return h;
}

rbnode* d_move_red_right(rbnode* h)
{
	// h는 레드이고, h.right와 h.right.left는 블랙이라고 가정한다.
	d_flip_colors(h);

	if (d_isred(h->left->left)) { // 왼쪽 자식이 3-노드일 때,
		h = d_rotateRight(h);
		d_flip_colors(h);
	}

}

void d_delete_min()
{
	printf("start delete min root!\n");
	if (!d_isred(bst_root->left) && !d_isred(bst_root->right)) {
		bst_root->color = RED;
	}
	bst_root = d_delete_min_impl(bst_root);
	if (bst_root) bst_root->color = BLACK;
}

rbnode* d_delete_min_impl(rbnode* h)
{
	printf("start delete min impl %s\n", h->key);
	// left 가 NULL 이라면 현재 요소가 제일 작은 요소라는 뜻.
	if (h->left == NULL) return NULL;

	// 2단계 연속으로 black 이거나 비어있을 때
	// 만약 현재 노드가 Red이든 Black이든, 그 레벨에서 가장 작은 곳까지 다다를 때까지 움직임.
	// h->left가 Black이어도, h->left->left가 Red라면 아무런 작업을 하지 않음.
	if (!d_isred(h->left) && !d_isred(h->left->left)) h = d_move_red_left(h);
	h->left = d_delete_min_impl(h->left);
	return d_balance(h);
}

void d_delete_max()
{
	if (!d_isred(bst_root->left) && !d_isred(bst_root->right)) 
		bst_root->color = RED;
	bst_root = d_delete_max_impl(bst_root);
	if (bst_root) bst_root->color = BLACK;


}

rbnode* d_delete_max_impl(rbnode* h)
{
	printf("start delete max impl %s\n", h->key);
	if (d_isred(h->left)) { // 만약 정상적인 3-노드일 때 오른쪽으로 치우치게 한다.
		h = d_rotateRight(h);
	}
	if (h->right == NULL) {
		return NULL;
	}

	if (!d_isred(h->right) && !d_isred(h->right->left)) {
		h = d_move_red_right(h);
	}
	h->right = d_delete_max_impl(h->right);
	return d_balance(h);
}

void d_delete(rbKey key)
{
	// 추후 색반전을 위해 미리 보정함.
	if (!d_isred(bst_root->left) && !d_isred(bst_root->right)) bst_root->color = RED;
	bst_root = d_delete_impl(bst_root, key);
	if (bst_root) bst_root->color = RED;
}

rbnode* d_delete_impl(rbnode* h, rbKey key) {

	if (strcmp(key, h->key) < 0) {
		if (!d_isred(h->left) && !d_isred(h->left->left))
			h = d_move_red_left(h);
		h->left = d_delete_impl(h->left, key);
	}
	else {
		if (d_isred(h->left)) h = d_rotateRight(h); // 왼쪽에 있는 
		if (strcmp(key, h->key) == 0 && h->right == NULL) // 만약 찾았고 오른편에 아무것도 없을 때
			return NULL;
		if (!d_isred(h->right) && !d_isred(h->right->left))
			h = d_move_red_right(h);
		if (strcmp(key, h->key) == 0) { // 만약 찾았지만, 중간에 있는 키일 경우
			// 근후행 노드의 값으로 현재 노드의 값을 대체하고 (현재 노드는 어차피 사라지니까)
			// 근후행 노드를 삭제한다.
			rbnode* x = d_min(h->right);
			h->key = x->key;
			h->value = x->value;
			h->right = d_delete_min_impl(h->right);
		}
		else h->right = d_delete_impl(h->right, key);
	}
	return d_balance(h);
}

rbnode* d_balance(rbnode* h)
{
	printf("balencing %s\n", h->key);
	if (d_isred(h->right)) {
		h = d_rotateLeft(h);
	}
	if (d_isred(h->left) && d_isred(h->left->left)) {
		h = d_rotateRight(h);
	}
	if (d_isred(h->left) && d_isred(h->right)) {
		d_flip_colors(h);
	}
	h->sub_count = d_size(h->left) + d_size(h->right) + 1;
	return h;
}

void d_flip_colors(rbnode* h)
{
	printf("fliped color h:%s, h->left:%s, h->right:%s\n", h->key, h->left->key, h->right->key);
	h->color = !h->color;
	h->left->color = !h->left->color;
	h->right->color = !h->right->color;
}

void d_printall()
{
	print_lv = 0;
	if (bst_root) d_print(bst_root);
}

void d_add_wordcount(rbKey key)
{
	int* value = d_get(key);
	if (value) *value += 1;
	else {
		int* i = (int*)malloc(sizeof(int));
		*i = 1;
		tree_put(key, i);
		printf("key %s added!\n", key);
	}
}

void d_print(rbnode* node) {
	if (node == NULL) return;
	if (node->color == BLACK) print_lv++;
	char c = node->color == BLACK ? 'B' : 'R';
	d_print(node->left);
	printf("[%c]%d:%s\n", c, print_lv, node->key/*, *(int*)node->value*/);
	d_print(node->right);
	if (node->color == BLACK) print_lv--;
}


