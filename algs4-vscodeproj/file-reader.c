#include "file-reader.h"
#include <ctype.h>
#include <stdlib.h>

// max one string size
#define STR_BUFFER_SIZE 20

// read int from file. returns INT_MIN when it's EOF.
void f_readInt(FILE* fp, int* opt)
{	
	char buffer[50];
	char c;
	int i;
	while (!isdigit(c = (char)fgetc(fp))) {
		if (c == EOF) {
			*opt = INT_MIN;
			return;
		}
	}
	buffer[0] = c;
	int pos = 1;
	while (isdigit(c = (char)fgetc(fp))) {
		buffer[pos] = c;
		pos++;
	}
	buffer[pos] = '\0';
	return atoi(buffer);
}

// if it fails, buffer[0] will be set to '\0'
// return length

char* f_readString_mkptr(FILE* fp) {
	int max = 6;
	char* str = (char*)malloc(sizeof(char) * max);
	char c;
	while (!isalpha(c = (char)fgetc(fp))) {
		if (c == EOF) {
			str[0] = '\0';
			return str;
		}
	}
	str[0] = c;
	int pos = 1;
	while (isalpha(c = (char)fgetc(fp))) {
		str[pos] = c;
		pos++;
		if (pos > max - 3) {
			max *= 2;
			char* realloced = (char*)realloc(str, sizeof(char) * max);
			if (realloced != NULL) str = realloced;
		}
	}
	str[pos] = '\0';
	return str;
}
int f_readString(char* buffer, FILE* fp)
{
	char c;
	int i;
	while (!isalpha(c = (char)fgetc(fp))) {
		if (c == EOF) {
			buffer[0] = '\0';
			return;
		}
	}
	buffer[0] = c;
	int pos = 1;
	while (isalpha(c = (char)fgetc(fp))) {
		buffer[pos] = c;
		pos++;
	}
	buffer[pos] = '\0';
	return pos;
}

// uses malloc, but there's no allocaltion free .
void f_readAllStrings(FILE* fp, char*** opt_strings, int* opt_count)
{
	int max_count = 10;
	char** output = (char**)malloc(sizeof(char*) * max_count);
	int i = 0;
	while (!feof(fp)) {
		char* buffer = (char*)malloc(sizeof(char) * STR_BUFFER_SIZE);
		f_readString(buffer, fp);
		
		// if file ends, end loop.
		if (buffer[0] == '\0') break;
		
		output[i] = buffer;
		

		// 만약 절반을 넘으면 버퍼 크기를 2배한다.
		i++;
		if (i > max_count / 2) {
			max_count *= 2;
			output = (char**)realloc(output, sizeof(char*) * max_count);
		}

	}
	*opt_strings = output;
	*opt_count = i;
}


void f_readAllInts(FILE* fp, int** opt_ints, int* opt_count) {
	/*int max_count = 10;
	int* output = (int*)malloc(sizeof(int) * max_count);
	int i = 0;
	while (!feof(fp)) {
		int number;
		f_readInt(buffer, fp);

		if (buffer[0] == '\0') break;

		output[i] = buffer;
		//printf("%d: %s\n", i, buffer);

		// 만약 절반을 넘으면 버퍼 크기를 2배한다.
		i++;
		if (i > max_count / 2) {
			max_count *= 2;
			output = (char**)realloc(output, sizeof(char*) * max_count);
		}
	}*/
}
