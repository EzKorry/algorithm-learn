#pragma once
#include <stdbool.h>
#include <stdio.h>


const bool BLACK;
const bool RED;

int print_lv;

typedef struct _rbnode {
	char* key;
	void* value;
	struct _rbnode* left;
	struct _rbnode* right;
	int sub_count;
	bool color;
} rbnode;

typedef char* rbKey;
typedef void* rbValue;



rbnode* bst_root;

rbnode* rbNode(rbKey key, rbValue value, int sub_count, bool color);

bool d_isred(rbnode* h);

rbnode* d_rotateLeft(rbnode* h);

rbnode* d_rotateRight(rbnode* h);

int d_size(rbnode* h);

void tree_put(rbKey key, rbValue value);

rbnode* d_put(rbnode* node, rbKey key, rbValue value);

bool d_contains(rbKey key);

void* d_get(rbKey key);

rbnode* d_min(rbnode* h);

rbnode* d_find(rbnode* x, rbKey key);

rbnode* d_find_root(rbKey key);

rbnode* d_move_red_left(rbnode* h);
rbnode* d_move_red_right(rbnode* h);

void d_delete_min();
rbnode* d_delete_min_impl(rbnode* h);

void d_delete_max();
rbnode* d_delete_max_impl(rbnode* h);

void d_delete(rbKey key);
rbnode* d_delete_impl(rbnode* h, rbKey key);

rbnode* d_balance(rbnode* h);

void d_flip_colors(rbnode* h);

void d_printall();

void d_add_wordcount(rbKey key);
/*
void d_printall_pretty();

int d_print_pretty(rbnode* node);*/

void d_print(rbnode* node);

