#pragma once
#include <stdbool.h>

int uf_count;
int uf_length;
int* uf_id;
int* uf_sz;

void uf_init(int n);

int uf_get_count();

bool uf_connected(int p, int q);

int uf_find(int p);

void uf_union(int p, int q);

